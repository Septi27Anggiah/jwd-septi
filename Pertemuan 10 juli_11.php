<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Operator Ternary</title>
    </head>
    <body>
        <script>
            var pertanyaan = confirm("Apakah Anda sudah move on?");

            var hasil = pertanyaan ? "Silahkan Anda lanjutkan dengan yang lain" : "Silahkan lupakan masa lalu dan tak perlu memikirnya kembali";
            document.write(hasil);
        </script>
    </body>
</html>
