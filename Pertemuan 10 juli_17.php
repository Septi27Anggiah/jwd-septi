<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Perulangan foreach dan repeat</title>
    </head>
    <body>
        <script>
            var days = ["Senin", "Selasa", "Rabu", "Kamis"];

            //kemudian kita tampilkan semua hari dengan metode foreach
            days.forEach((day) => {
                document.write("<p>" + day + "</p>");
            });

            //Contoh penggunaan repeat
            for( let i = 0; i < 100; i++) {
                document.write("Ulangi kalimat ini!".repeat(100));
            }
        </script>
    </body>
</html>