<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Percabangan Switch/case</title>
    </head>
    <body>
        <script>
            var jawab = prompt("Kamu beruntung! Silahkan pilih jodohmu dnegan memaasukan angka 1 sampai 5");
            var hadiah = "";

            switch (jawab) {
                case "1":
                    hadiah = "Sholeh";
                    break;
                case "2":
                    hadiah = "Setia";
                    break;
                case "3":
                    hadiah = "Tanggungjawab";
                    break;
                case "4":
                    hadiah = "Dermawan";
                    break;
                case "5":
                    hadiah = "Mandiri";
                    break;
                default:
                    document.write("<p>Opss! Anda salah pilih jodoh</p>");
            }
            
            if (hadiah === "") {
                document.write("<p>Kamu gagal mendapatkan jodoh</p>");
            } else {
                document.write("<h2>Selamat kamu mendapatkan jodoh " + hadiah + "</h2>");
            }
        </script>
    </body>
</html>
