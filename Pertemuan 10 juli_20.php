<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Perulangan foreach dan repeat</title>
    </head>
    <body>
        <script>
            var ulangi = confirm("Apakah Anda ingin cepat lulus?");
            var counter = 0;

            while (ulangi) {
                counter++;
                var bintang = "uang".repeat(counter) + "<br>";
                document.body.insertAdjacentHTML('beforeend', counter + ":" + bintang);
                ulangi = confirm("Apakah ingin lulus?");
            }
        </script>
    </body>
</html>
