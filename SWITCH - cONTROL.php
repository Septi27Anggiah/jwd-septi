SWITCH - cONTROL
<html>
   <body>
<?php
$plat_nomor = "H";
switch($plat_nomor){
   case "AB":
      echo "Yogyakarta";
      break;
   case "AD":
      echo "Surakarta";
      break;
   case "BE":
      echo "Lampung";
      break;
   case "B":
      echo "Jakarta";
      break;
   default:
      echo "Plat kendaraan tidak diketahui.";
      break;
}
?>
<br>
      <?php
         $d = date("D");
         
         if ($d == "Fri")    //tidak diakhiri titik koma
            echo "Have a nice weekend!";
         elseif ($d == "Sun")     // tidak diakhiri titik koma
            echo "Have a nice Sunday!"; 
         else      // tidak diakhiri titik koma
            echo "Have a nice day!"; 
      ?>
<br>
<?php
	$nilai = 40;
	if($nilai >= 60){
		echo "Selamat anda lulus!";
	}else {
		echo "Coba lagi semester depan.";
	}
?>
<?php
     

?>
</body>
</html>
Total Nilai = N1+N2+N3
Nilai Rata-rata = Total Nilai / 3

Ketentuan Nilai Huruf dan Predikat
Jika Nilai rata > 89 maka Nilai Huruf = A, Predikat = Memuaskan
Jika Nilai rata > 79 maka Nilai Huruf = B, Predikat = Baik
Jika Nilai rata > 69 maka Nilai Huruf = C, Predikat = Cukup
Jika Nilai rata > 59 maka Nilai Huruf = D, Predikat = Kurang
Jika Nilai rata 0-59 maka Nilai Huruf = E, Predikat = Gagal