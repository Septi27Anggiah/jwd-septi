<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Internal CSS</title>
	<style type="text/css">
		body{
			background-color: lightgrey;
		}

		/*h1{
			color : white;
			text-align: center;
		}*/

		p{
			font-family: verdana;
			font-size: 20px;
		}

		.myClass{
			font: bold 1.25em times;
			color: red;
		}
	</style>
</head>
<body>
	Latihan CSS 1

	<h1>Ini adalah implementasi H1</h1>

	<p>Ini adalah implementasi tag p / paragraf</p>

	<h1 class="myClass">Ini adalah H1 myclass</h1>
	<p class="myClass">Ini adalah P myclass</p>
</body>
</html>